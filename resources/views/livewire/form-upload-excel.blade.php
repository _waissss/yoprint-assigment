<div>
    <form class="form fv-plugins-bootstrap5 fv-plugins-framework" wire:submit="save">
        <!--begin::Input group-->
        <div class="row mb-6">
            <!--begin::Label-->
            <label class="col-lg-4 col-form-label required fw-semibold fs-6">Upload Files</label>
            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-8">
                <input type="file" wire:model="excel_file" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" multiple>
                @error('excel_file')
                    <div class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">{{ $message }}</div>
                @enderror
                @error('excel_file.*')
                    <div class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <!--end::Col-->
        </div>
        <!--end::Input group-->
        <!--begin::Actions-->
        <div class="d-flex justify-content-end py-6 px-9 mb-10">
            <button wire:loading.remove wire:target="save" type="submit" class="btn btn-primary">Upload</button>
            <button wire:loading wire:target="save" class="btn btn-primary">
                <span>
                    Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
            </button>
        </div>
        <!--end::Actions-->
    </form>
</div>
