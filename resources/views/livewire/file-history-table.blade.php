<div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr class="fw-bold fs-6 text-gray-800">
                    <th>Time</th>
                    <th>File Name</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody wire:poll>
                @foreach ($filehistories as $history)
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($history->created_at)->format('j M Y g:i A') }}<br>({{ \Carbon\Carbon::parse($history->created_at)->diffForHumans() }})</td>
                        <td>{{ $history->filename }}</td>
                        <td>{{ $history->status }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
