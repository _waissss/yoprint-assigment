<?php

namespace App\Livewire;

use App\Models\FileHistory;
use Livewire\Component;

class FileHistoryTable extends Component
{
    public function render()
    {
        $filehistories = FileHistory::all();

        return view('livewire.file-history-table', compact('filehistories'));
    }
}
