<?php

namespace App\Livewire;

use App\Imports\ExcelImport;
use App\Jobs\ImportExcelJob;
use App\Models\FileHistory;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class FormUploadExcel extends Component
{
    use WithFileUploads;

    public $excel_file = [];

    public function render()
    {
        return view('livewire.form-upload-excel');
    }

    public function save()
    {
        $this->validate([
            'excel_file' => 'required',
            'excel_file.*' => 'mimes:csv,xls,xlsx'
        ]);

        foreach($this->excel_file as $file){
            $filename = $file->getClientOriginalName();

            $path = $file->storeAs(
                'excel_files',
                $filename,
                'public'
            );

            $file = FileHistory::create([
                'filename' => $filename,
                'path' => $path
            ]);

            ImportExcelJob::dispatch($file, $path);
        }

        $this->excel_file = [];

        $this->dispatch('swal:modal', type: "success", title: "Great!", text: "Success Uploaded Excel Files!");
        
    }
}
