<?php

namespace App\Jobs;

use App\Imports\ExcelImport;
use App\Models\FileHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportExcelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $path, $file;

    /**
     * Create a new job instance.
     */
    public function __construct(FileHistory $file, $path)
    {
        $this->path = $path;
        $this->file = $file;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->file->update(['status' => 'processing']);

        try {

            (new ExcelImport)->import($this->path, 'public', \Maatwebsite\Excel\Excel::CSV);

            $this->file->update(['status' => 'completed']);

        } catch (\Exception $e) {

            \Log::error('Error importing Excel file: ' . $e->getMessage());
            $this->file->update(['status' => 'failed']);
        }
    }
}
