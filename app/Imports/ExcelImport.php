<?php

namespace App\Imports;

use App\Models\UploadFile;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ExcelImport implements ToModel, WithUpserts, WithBatchInserts, WithStartRow, WithCustomCsvSettings
{
    use Importable;

     /**
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'unique_key';
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new UploadFile([
            'unique_key' => $row[0],
            'product_title' => $row[1],
            'product_description' => $row[2],
            'style_number' => $row[3],
            'mainframe_color' => $row[28],
            'size' => $row[18],
            'color_name' => $row[14],
            'piece_price' => $row[21]
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function startRow(): int
    {
        return 2; // Skip the first row (header row)
    }

    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'UTF-8'
        ];
    }
}
